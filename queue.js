let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.
// #1
const print = () => {
	return collection;
};

// #2 #3 #5 #6
const enqueue = (name) => {

	collection[collection.length] = name;
	return collection;
};

// #4
const dequeue = () => {
	for (let i = 1; i < collection.length; i++) {
		collection[i - 1] = collection[i]
	};
	collection.length--;
	return collection;
}

// #7
const front = () => {
	return collection[0];
}

// #8
const size = () => {
	return collection.length;
}

// #9
const isEmpty = () => {
	return size() === 0;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
	//export created queue functions
};